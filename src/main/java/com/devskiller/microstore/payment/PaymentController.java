package com.devskiller.microstore.payment;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
class PaymentController {

	private final PaymentService paymentService;

	PaymentController(PaymentService paymentService) {
		this.paymentService = paymentService;
	}

	@RequestMapping(value = "/payments/{orderId}",
			method = RequestMethod.PUT,
			headers = "Accept=application/json"
	)
	ResponseEntity<Object> receivePayment(@PathVariable @Nullable String orderId,
										  @RequestBody PaymentRequest paymentRequest) {
		paymentService.processOrderOnPayment(orderId, paymentRequest);
		return new ResponseEntity<>(HttpStatus.ACCEPTED);
	}



}
