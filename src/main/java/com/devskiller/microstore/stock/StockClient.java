package com.devskiller.microstore.stock;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "stock")
interface StockClient {


	@RequestMapping(method = RequestMethod.GET,
			value = "/products/{orderId}",
			produces = "application/json")
	StockResponse checkStock (@PathVariable @Nullable String orderId);

}
