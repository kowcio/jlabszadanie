package com.devskiller.microstore;

import com.devskiller.microstore.warehouse.WarehouseQueue;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.cloud.stream.annotation.EnableBinding;

@SpringBootApplication
@EnableFeignClients
@EnableBinding(WarehouseQueue.class)
@EnableDiscoveryClient
public class MicrostoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(MicrostoreApplication.class, args);
	}



}
