package com.devskiller.microstore.warehouse;

public class WarehouseResponse {

    private String orderId;
    private String action;

    public WarehouseResponse(String orderId, String action) {
        this.orderId = orderId;
        this.action = action;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getAction() {
        return action;
    }
}
