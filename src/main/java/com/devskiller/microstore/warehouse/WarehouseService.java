package com.devskiller.microstore.warehouse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
@EnableBinding(Source.class)
public class WarehouseService {

	@Autowired
	private WarehouseQueue warehouseQueue;

	public void sendPackage(String orderId) {

		WarehouseResponse wr = new WarehouseResponse(orderId, "SEND");

		Message<WarehouseResponse> contentType = MessageBuilder
				.withPayload(wr)
				.setHeader("contentType", "application/json")
				.build();
		warehouseQueue.output().send(contentType);

	}

}
